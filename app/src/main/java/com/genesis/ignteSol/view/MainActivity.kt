package com.genesis.ignteSol.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.genesis.ignteSol.R
import com.genesis.ignteSol.model.adapter.GenreAdapter
import com.genesis.ignteSol.model.Genre
import com.genesis.ignteSol.model.utils.GenreClickListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),
    GenreClickListener {
    private lateinit var genreList: MutableList<Genre>
    private lateinit var genreAdapter: GenreAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        genreList = ArrayList()
        genreAdapter = GenreAdapter(
            genreList as ArrayList<Genre>,
            this,
            this
        )
        genreRecyclerView.adapter = genreAdapter
        loadCategoryData()
    }

    private fun loadCategoryData() {
        var genre =
            Genre(R.drawable.ic_fiction, "Fiction")
        genreList.add(genre)
        genre = Genre(R.drawable.ic_drama, "Drama")
        genreList.add(genre)
        genre = Genre(R.drawable.ic_humor, "Humor")
        genreList.add(genre)
        genre =
            Genre(R.drawable.ic_politics, "Politics")
        genreList.add(genre)
        genre = Genre(
            R.drawable.ic_philosophy,
            "Philosophy"
        )
        genreList.add(genre)
        genre =
            Genre(R.drawable.ic_history, "History")
        genreList.add(genre)
        genre = Genre(
            R.drawable.ic_adventure,
            "Adventure"
        )
        genreList.add(genre)
        genreAdapter.notifyDataSetChanged()
    }

    override fun onGenreClicked(genreName: String) {
        val intent = Intent(this, SecondActivity::class.java)
        intent.putExtra("name", genreName)
        startActivity(intent)
    }
}
