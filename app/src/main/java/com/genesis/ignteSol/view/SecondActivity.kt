package com.genesis.ignteSol.view

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.genesis.ignteSol.R
import com.genesis.ignteSol.controller.Controller
import com.genesis.ignteSol.model.Book
import com.genesis.ignteSol.model.adapter.BookAdapter
import com.genesis.ignteSol.model.utils.BookClickListener
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity(), BookClickListener, OnEditorActionListener,
    Controller.CallbackListener {
    private lateinit var bookList: MutableList<Book>
    private lateinit var bookAdapter: BookAdapter
    private lateinit var mController: Controller
    private lateinit var genreName: String
    private lateinit var formatString: String
    private var noFormat = false
    private var next = false
    private var count: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        mController = Controller(this, this)
        genreName = intent.getStringExtra("name")!!
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener { finish() }
        name.text = genreName

        search_editText.setOnEditorActionListener(this)

        search_layout.setEndIconOnClickListener {
            search_editText.text = null
            bookList.clear()
            mController.loadBookData(genreName, count)
        }

        bookList = ArrayList()
        bookAdapter = BookAdapter(
            bookList as ArrayList<Book>,
            this,
            this
        )
        val mLayoutManager = GridLayoutManager(this, 3)
        bookRecyclerView.layoutManager = mLayoutManager
        bookRecyclerView.adapter = bookAdapter

        mController.loadBookData(genreName, count)

        var loading = true
        var pastVisiblesItems: Int
        var visibleItemCount: Int
        var totalItemCount: Int

        bookRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) { //check for scroll down
                    visibleItemCount = mLayoutManager.childCount
                    totalItemCount = mLayoutManager.itemCount
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition()
                    if (loading) {
                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                            progressBar.visibility = View.VISIBLE
                            loading = false
                            if (next) {
                                count++
                                mController.loadBookData(genreName, count)
                            }
                        }
                    }
                }
            }
        })
    }

    override fun onBookClicked() {
        if (noFormat) {
            val alertDialog = AlertDialog.Builder(this)
            alertDialog.setTitle("Sorry")
            alertDialog.setMessage("No viewable format available")
            alertDialog.setPositiveButton("Ok") { _, _ -> }
            val alert = alertDialog.create()
            alert.show()
        } else {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(formatString))
            browserIntent.setPackage("com.android.chrome")
            startActivity(browserIntent)
        }
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_GO) {
            count = 1
            bookList.clear()
            mController.loadBookData(genreName + " " + v?.text.toString(), count)
            val inm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inm.hideSoftInputFromWindow(search_editText.windowToken, 0)
            return true
        }
        return false
    }

    override fun loadDataAdapter(
        book: Book,
        formatString: String,
        noFormat: Boolean
    ) {
        bookList.add(book)
        bookAdapter.notifyDataSetChanged()
        this.formatString = formatString
        this.noFormat = noFormat
    }

    override fun loadDataFailed() {
        Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show()
    }

    override fun loadDataComplete(next: Boolean) {
        progressBar.visibility = View.GONE
        this.next = next
    }
}
