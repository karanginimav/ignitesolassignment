package com.genesis.ignteSol.controller

import android.content.Context
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.genesis.ignteSol.model.Book
import com.genesis.ignteSol.model.utils.Constants
import java.net.URLEncoder
import java.util.*
import kotlin.collections.ArrayList

class Controller(private val callbackListener: CallbackListener, private val context: Context) {
    private lateinit var requestQueue: RequestQueue
    private var bookList = ArrayList<Book>()
    private lateinit var formatString: String
    private var noFormat = false
    private var next = false

    interface CallbackListener {
        fun loadDataAdapter(
            book: Book,
            formatString: String,
            noFormat: Boolean
        )

        fun loadDataFailed()
        fun loadDataComplete(next: Boolean)
    }

    fun loadBookData(genreName: String, count: Int) {
        requestQueue = Volley.newRequestQueue(context)
        val stringURL = if (count == 1) {
            "&search=" + URLEncoder.encode(
                genreName.toLowerCase(Locale.getDefault()),
                "UTF-8"
            )
        } else {
            "&page=$count&search=" + URLEncoder.encode(
                genreName.toLowerCase(Locale.getDefault()), "UTF-8"
            )
        }

        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET,
            Constants.IMAGE_URL + stringURL, null,
            Response.Listener {
                val jsonArray = it.getJSONArray("results")
                for (i in 0 until jsonArray.length()) {
                    val book = Book()
                    val resultObject = jsonArray.getJSONObject(i)
                    val authorArray = resultObject.getJSONArray("authors")
                    if (authorArray.length() > 0) {
                        for (j in 0 until authorArray.length()) {
                            book.bookAuthor = authorArray.getJSONObject(j).getString("name")
                        }
                    } else {
                        book.bookAuthor = ""
                    }
                    val formatObject = resultObject.getJSONObject("formats")
                    if (formatObject.has("image/jpeg")) {
                        book.bookImage = formatObject.getString("image/jpeg")
                    } else {
                        book.bookImage = ""
                    }
                    when {
                        formatObject.length() == 0 -> {
                            noFormat = true
                        }
                        formatObject.has("text/html; charset=iso-8859-1") -> {
                            formatString = formatObject.getString("text/html; charset=iso-8859-1")
                        }
                        formatObject.has("text/html; charset=utf-8") -> {
                            formatString = formatObject.getString("text/html; charset=utf-8")
                        }
                        formatObject.has("text/html") -> {
                            formatString = formatObject.getString("text/html")
                        }
                        formatObject.has("application/pdf") -> {
                            formatString = formatObject.getString("application/pdf")
                        }
                        formatObject.has("text/plain") -> {
                            formatString = formatObject.getString("text/plain")
                        }
                        formatObject.has("text/plain; charset=utf-8") -> {
                            formatString = formatObject.getString("text/plain; charset=utf-8")
                        }
                        formatObject.has("text/plain; charset=us-ascii") -> {
                            formatString = formatObject.getString("text/plain; charset=us-ascii")
                        }
                    }
                    if (resultObject.has("title")) {
                        book.bookTitle = resultObject.getString("title")
                    } else {
                        book.bookTitle = ""
                    }
                    callbackListener.loadDataAdapter(book, formatString, noFormat)
                    bookList.add(book)
                }
                val nextString = it.getString("next")
                if (!nextString.isNullOrEmpty() && !nextString.contains("null")) {
                    next = true
                }
                callbackListener.loadDataComplete(next)
            },
            Response.ErrorListener {
                callbackListener.loadDataFailed()
            })
        requestQueue.add(jsonObjectRequest)
    }
}
