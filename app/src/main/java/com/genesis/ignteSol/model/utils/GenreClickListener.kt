package com.genesis.ignteSol.model.utils

interface GenreClickListener {
    fun onGenreClicked(genreName: String)
}
