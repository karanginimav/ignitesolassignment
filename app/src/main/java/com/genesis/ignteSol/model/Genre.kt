package com.genesis.ignteSol.model

data class Genre(
    val genreImage: Int,
    val genreName: String
)
