package com.genesis.ignteSol.model.utils

object Constants {
    @JvmField
    var BASE_URL = "http://skunkworks.ignitesol.com:8000/books"
    var IMAGE_URL = "$BASE_URL/?mime_type=image%2F"
}