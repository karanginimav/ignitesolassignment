package com.genesis.ignteSol.model.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.genesis.ignteSol.model.utils.GenreClickListener
import com.genesis.ignteSol.R
import com.genesis.ignteSol.model.Genre
import kotlinx.android.synthetic.main.genre_card.view.*

class GenreAdapter(
    private val genreList: ArrayList<Genre>,
    private val context: Context,
    private val genreClickListener: GenreClickListener
) :
    RecyclerView.Adapter<GenreAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.genre_card, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return genreList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val genre = genreList[position]
        holder.genreImage.setImageResource(genre.genreImage)
        holder.genreName.text = genre.genreName
        holder.itemView.setOnClickListener {
            genreClickListener.onGenreClicked(genre.genreName)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var genreImage: ImageView = itemView.genre_image
        var genreName: TextView = itemView.genre_name
    }
}


