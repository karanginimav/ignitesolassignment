package com.genesis.ignteSol.model.utils

interface BookClickListener {
    fun onBookClicked()
}
