package com.genesis.ignteSol.model.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.genesis.ignteSol.model.utils.BookClickListener
import com.genesis.ignteSol.R
import com.genesis.ignteSol.model.Book
import kotlinx.android.synthetic.main.book_card.view.*

class BookAdapter(
    private val bookList: ArrayList<Book>,
    private val context: Context,
    private val bookClickListener: BookClickListener
) :
    RecyclerView.Adapter<BookAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.book_card, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return bookList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val book = bookList[position]
        Glide.with(context).load(book.bookImage).into(holder.bookImage)
        holder.bookTitle.text = book.bookTitle
        holder.bookAuthor.text = book.bookAuthor
        holder.itemView.setOnClickListener {
            bookClickListener.onBookClicked()
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var bookImage: ImageView = itemView.book_image
        var bookTitle: TextView = itemView.book_title
        var bookAuthor: TextView = itemView.book_author
    }
}
