package com.genesis.ignteSol.model

class Book {
    var bookImage: String = ""
    var bookTitle: String = ""
    var bookAuthor: String = ""
}
